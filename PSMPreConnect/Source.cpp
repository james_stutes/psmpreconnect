// Demonstration of a specific (PSMSessionStartTime) session parameter retrieving using PSMUserExitAPI 

#include "PSMUserExitUtils.h"
#include <stdio.h>
#include <Windows.h>

const short int PSM_RC_SUCCESS = 0;
const short int PSM_RC_FAILURE = -1;
const short int LOG_MESSAGE_MAX_SIZE = 300;

const short int NUMBER_STRINGS = 35;

int main()

{
	char szLogMessage[LOG_MESSAGE_MAX_SIZE];
	char* szPropertyValueBuffer = NULL;
	DWORD dwPropertyValueLength;
	// int nPropertyNameLength = strlen(szPropertyName);
	int nRc = PSM_RC_SUCCESS;

	char *arrayPropertyName[NUMBER_STRINGS];
	arrayPropertyName[0] = "Address";
	arrayPropertyName[1] = "CPMErrorDetails";
	arrayPropertyName[2] = "CPMStatus";
	arrayPropertyName[3] = "CreationMethod";
	arrayPropertyName[4] = "DeviceType";
	arrayPropertyName[5] = "LastFailDate";
	arrayPropertyName[6] = "LastSuccessChange";
	arrayPropertyName[7] = "LastSuccessVerification";
	arrayPropertyName[8] = "LastTask";
	arrayPropertyName[9] = "PolicyID";
	arrayPropertyName[10] = "RetriesCount";
	arrayPropertyName[11] = "UserName";
	arrayPropertyName[12] = "AccountSafeName";
	arrayPropertyName[13] = "PSMClientApp";
	arrayPropertyName[14] = "PSMComponentsFolder";
	arrayPropertyName[15] = "PSMComponentsLogFolder";
	arrayPropertyName[16] = "PSMSessionStartTime";
	arrayPropertyName[17] = "PSMSessionUUID";
	arrayPropertyName[18] = "RemoteApp";
	arrayPropertyName[19] = "AccessReason";
	arrayPropertyName[20] = "ClientUserName";
	arrayPropertyName[21] = "IsAdminSession";
	arrayPropertyName[22] = "IsRemoteApp";
	arrayPropertyName[23] = "PSMSourceAddress";
	arrayPropertyName[24] = "PVWAConnectTime";
	arrayPropertyName[25] = "SelectedConnCompID";
	arrayPropertyName[26] = "AutoLogonSequenceWithLogonAccount";
	arrayPropertyName[27] = "EnableXForwarding";
	arrayPropertyName[28] = "port";
	arrayPropertyName[29] = "ShellPromptForAudit";
	arrayPropertyName[30] = "TerminateOnShellPromptFailure";
	arrayPropertyName[31] = "XServerCommandLine";
	arrayPropertyName[32] = "BusinessEmail";
	arrayPropertyName[33] = "HomeEmail";
	arrayPropertyName[34] = "OtherEmail";



	// Validate that the DLL was successfully loaded 
	if (!InitializeUserExitUtils())
	{
		nRc = PSM_RC_FAILURE;
	}
	else
	{

		for (int i = 0; i < NUMBER_STRINGS; i++)
		{
			// Using PSMUserExitAPI to retrieve the property value's length in order to know how much memory is needed to store the value
			// If the property doesn't exist, the function will fail 

			// Create the error log 
			sprintf_s(szLogMessage, LOG_MESSAGE_MAX_SIZE, "Index=%d", i);
			// Write the log using PSMUserExitAPI 
			WriteLog(szLogMessage, strlen(szLogMessage), 0);

			if (!GetSessionPropertyBufferLength(arrayPropertyName[i], strlen(arrayPropertyName[i]), &dwPropertyValueLength))
			{
				// Create the error log 
				sprintf_s(szLogMessage, LOG_MESSAGE_MAX_SIZE, "%s was notfound", arrayPropertyName[i]);
				// Write the log using PSMUserExitAPI 
				WriteLog(szLogMessage, strlen(szLogMessage), 0);
				// nRc = PSM_RC_FAILURE;
			}
			else
			{
				// create the trace log 
				sprintf_s(szLogMessage, LOG_MESSAGE_MAX_SIZE, "GetSessionPropertyBufferLength for %s has succeeded, length: [%lu]", arrayPropertyName[i], dwPropertyValueLength);
				// Write the log using PSMUserExitAPI 
				WriteLog(szLogMessage, strlen(szLogMessage), 1);

				// Allocate the required memory for storing the session proprty value
				szPropertyValueBuffer = new char[dwPropertyValueLength];

				// Getting the session property's value (the szPropertyValueBuffer will be filled) using PSMUserExitAPI 
				if (GetSessionProperty(arrayPropertyName[i], strlen(arrayPropertyName[i]), szPropertyValueBuffer, dwPropertyValueLength))
				{
					// Create the trace log 
					sprintf_s(szLogMessage, LOG_MESSAGE_MAX_SIZE, "PSMPreConnect => %s = [%s]", arrayPropertyName[i], szPropertyValueBuffer);
					// Write the log using PSMUserExitAPI 
					WriteLog(szLogMessage, strlen(szLogMessage), 1);
				}
				delete[] szPropertyValueBuffer;
			}
		}

		// Release the allocated resources in the DLL (called automatically when the DLL is unloaded from the process) 
		if (!TerminateUserExitUtils())
		{
			// Create the error log 
			sprintf_s(szLogMessage, LOG_MESSAGE_MAX_SIZE, "PSMPreConnect User exit utils termination has failed");
			// Write the log using PSMUserExitUtils 
			WriteLog(szLogMessage, strlen(szLogMessage), 0);
		}

		// Create the trace log 
		//sprintf_s(szLogMessage, LOG_MESSAGE_MAX_SIZE, "PSMPreConnect User exit ended");
		// Write the log using PSMUserExitUtils 
		//WriteLog(szLogMessage, strlen(szLogMessage), 1);
	}

	return nRc;

}
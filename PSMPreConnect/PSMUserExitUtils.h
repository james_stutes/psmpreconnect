#ifndef PSMUSEREXITUTILS_H
#define PSMUSEREXITUTILS_H

//-------------------------------------------------------------------------------------
//                PSMUserExitUtils.h
//              ---------------------
//
// General	: Wraps the exported function in the PSMUserExitUtils DLL
//
// Note     : All the string parameters are passed with char* and size, in order to
//            support potential future wrappers (other languages other than c/c++)
//
// Author	: KerenY, August 2014
//-------------------------------------------------------------------------------------

// Summary:       Performing internal initialization. 
//                NOTICE - Must be called before the other functions.
//                         Called automatically by the DLL when loaded into the process.
//                         Can be called more than once.
//
// Parameters:    None.
//
// Return Value:  If the function succeeds, the return value is nonzero.
//                If the function fails, the return value is zero.
//
unsigned long InitializeUserExitUtils();


// Summary:       Performing internal memory deallocation.
//                NOTICE - Must be called before unloading the DLL.
//                         Called automatically by the DLL when unloaded from the process.
//                         Can be called more than once.
//
// Parameters:    None.
//
// Return Value:  If the function succeeds, the return value is nonzero.
//                If the function fails, the return value is zero.
//
unsigned long TerminateUserExitUtils();

// Summary:       Provide the buffer size in bytes needed to store the session property value.
//
// Parameters:    szPropertyName       [in]  - The request property name
//                ulPropertyNameLength [in]  - The length of szPropertyName, (NOT including '\0')
//                ulLength             [out] - The session property value length (including '\0')
//
// Return Value:  If the function succeeds, the return value is nonzero.
//                If the function fails, the return value is zero.
//
unsigned long GetSessionPropertyBufferLength(const char* szPropertyName, unsigned long ulPropertyNameLength, unsigned long* pulLength);

// Summary:       Provide the the session property value.
//
// Parameters:    szPropertyName       [in]  - The request property name
//                ulPropertyNameLength [in]  - The length of szPropertyName, (NOT including '\0')
//                szBuffer             [out] - The session property value (null-terminated)
//                ulBufferSize         [in]  - The size of szBuffer
//
// Warning:       The szBuffer must be large enough to contain the requested parameter value.
//
// Return Value:  If the function succeeds, the return value is nonzero.
//                If the function fails, the return value is zero.
//
unsigned long GetSessionProperty(const char* szPropertyName, unsigned long ulPropertyNameLength, char* szBuffer, unsigned long ulBufferSize);

// Summary:       Write a message in the log file.
//
// Parameters:    szMessage       [in] - A string message
//                ulMessageLength [in] - The length of szMessage, (NOT including '\0')
//                ulLogLevel      [in] - 0 for error log, 1 for trace log
//
// Return Value:  If the function succeeds, the return value is nonzero.
//                If the function fails, the return value is zero.
//
unsigned long WriteLog(const char* szMessage, unsigned long ulMessageLength, unsigned long ulLogLevel);

#endif PSMUSEREXITUTILS_H